using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Movement : MonoBehaviour
{
    [SerializeField] float speed = 12f;
    float gravity = -9.81f;
    
    [SerializeField] Transform groundCheck;
    [SerializeField] float groundDistance;
    [SerializeField] LayerMask groundMask;

    CharacterController controller;
    Transform mainCamera;

    Vector3 velocity;
    bool isGrounded;

    PlayerInputActions playerInputActions;



    bool isJumpPressed = false;
    bool isJumpHolding = false;
    [SerializeField] float maxJumpHeight = 1.5f;
    [SerializeField] float minJumpHeight = 1.0f;
    [SerializeField] float maxJumpTime = 0.8f;
    
    [SerializeField] float maxFallSpeed = 1.0f;
    float intialJumpVelocity;
    bool isJumping = false;

    [SerializeField] AnimationCurve jumpCurve;


    void Awake()
    {
        controller = GetComponent<CharacterController>();
        playerInputActions = new PlayerInputActions();

        playerInputActions.Player.Enable();
        playerInputActions.Player.Jump.performed += Jump;
        playerInputActions.Player.Jump.canceled += Jump;

        mainCamera = Camera.main.transform;

        SetupJumpVariables();
    }

    void SetupJumpVariables()
    {
        float timeToApex = maxJumpTime / 2;
        gravity = (-2 * maxJumpHeight) / Mathf.Pow(timeToApex, 2);
        intialJumpVelocity = (2 * maxJumpHeight) / timeToApex;
    }

    private void OnDrawGizmos() {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(groundCheck.position, groundDistance);
    }

    void FixedUpdate()
    {
        SetupJumpVariables();

        //Move by cam direction
        Vector2 inputVector = playerInputActions.Player.Movement.ReadValue<Vector2>();

        Vector3 move = mainCamera.right * inputVector.x + new Vector3(mainCamera.forward.x, 0, mainCamera.forward.z).normalized * inputVector.y;
        
        controller.Move(move * speed * Time.deltaTime);


        //Jump
        if (isGrounded && !isJumpPressed)
            velocity.y = 0f;
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        

        if (!isGrounded)
        {
            if (!isJumpHolding && velocity.y > 2f && velocity.y < 15f)
                velocity.y = 2f;
            else
                velocity.y = MathF.Max(velocity.y + (gravity * Time.deltaTime) * jumpCurve.Evaluate(velocity.y), -maxFallSpeed);
        }

        controller.Move((velocity + move * speed) * Time.deltaTime);
    }

    void Jump(InputAction.CallbackContext context)
    {
        if (context.ReadValueAsButton())
        {
            isJumpPressed = true;
            if (isGrounded)
            {
                isJumpHolding = true;
                velocity.y = intialJumpVelocity;
            }
        }
        else
        {
            isJumpPressed = false;
            isJumpHolding = false;
        }
    }
}